(function() {
	/*
	*	Partial gulp file: watch
	*	A gulp task for watching files and run tasks
	*/
	'use strict';

	/*
	*  Include dependencies
	*/
	var gulp = require('gulp');
	var config = require('./_config.js');

	/*
	*	Watch task
	*/
	gulp.task('watch', function() {

		// watch changes on sass files to compile sass to css
		gulp.watch(config.paths.in.css, ['css']);

		// watch javascript files to bundle them
		gulp.watch(config.paths.in.js, ['bundle:js']);
	});
})();