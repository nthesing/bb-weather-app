(function() {
	/*
	 *
	 *  Partial gulp file: build & bundling javascript
	 *  Build tasks
	 */
	'use strict';

	var errorHandler;

	/*
	 *  NPM Package file (for header information)
	 */
	var _package = require('../package.json');

	/*
	 *  Include dependencies
	 */
	var gulp = require('gulp');
	var config = require('./_config.js');
	var concat = require('gulp-concat');
	var uglify = require('gulp-uglify');
	var header = require('gulp-header');	
	var gulpif = require('gulp-if');
	var plumber	= require('gulp-plumber');
	var debug = require('gulp-debug');
	var angularFilesort = require('@pioug/gulp-angular-filesort');



	/*
	 ** Bundle javascript files
	 */
	gulp.task('bundle:js', function() {
		bundleJavascript(false);
	});

	/*
	 ** Bundle vendor files
	 */
	gulp.task('bundle:vendor', function() {
		bundleVendor();
	});

	// Function to bundle vendor
	function bundleVendor() {
		var scripts = [
			'vendor/angular/angular.min.js',
			'vendor/angular-resource/angular-resource.min.js',
			'vendor/moment/moment.js'
		]
		return gulp.src(scripts)
			.pipe(debug({ title: '### Vendor files:' }))
			.pipe(concat('vendor.bundle.js'))
			.pipe(plumber(errorHandler))
			.pipe(gulp.dest(config.paths.out.build))
	};


	// Function to bundle all javascript files
	// Parameter: 'doUglify' to enable uglify
	function bundleJavascript(doUglify) {
		header = require('gulp-header');

		return gulp.src(config.paths.in.js)
			.pipe(debug({ title: '### Javascript files:' }))
			.pipe(plumber(errorHandler))
			.pipe(angularFilesort())
			.pipe(concat('bundle.js'))
			.pipe(gulpif(doUglify, uglify({mangle: false})))
			.pipe(header(config.banner, {
				_package: _package
			}))
			.pipe(gulp.dest(config.paths.out.build))
	};
})();