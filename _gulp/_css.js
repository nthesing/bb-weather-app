(function() {
	/*
	 *
	 *  Partial gulp file: css
	 *  Different gulp css / styles tasks. Converting SASS to css
	 */
	'use strict';

	/*
	 *
	 *  Include dependencies
	 */
	var gulp = require('gulp');
	var header = require('gulp-header');
	var sass = require('gulp-sass');
	var autoprefixer = require('gulp-autoprefixer');
	var concat = require('gulp-concat');
	var gulpif = require('gulp-if');
	var minifyCSS = require('gulp-minify-css');
	var sourcemaps = require('gulp-sourcemaps');
	var config = require('./_config.js');
	var debug = require('gulp-debug');
	var gutil = require('gulp-util');
	
	/*
	 *  NPM Package file
	 */
	var _package = require('./../package.json');

	/*
	 *  CSS tasks (development & publish)
	 *   For development css:maps to include sourcemaps.
	 */
	gulp.task('css:maps', function() {
		return css(true, config.paths.out.build);
	});

	/*
	 *  CSS tasks (development & publish)
	 *   For build to include sourcemaps.
	 */
	gulp.task('css', function() {
		return css(false, config.paths.out.build);
	});

	/*
	* Converst sass to css, concat and reload.
	*/
	function css(sourceMaps, dest) {
		var stream = gulp.src(config.paths.in.css)
			.pipe(debug({ title: '### SASS files:' }))
			.pipe(sass({
				style: 'compressed'
			}))
			.on('error', function(err) {
				gutil.log(err);
				this.emit('end');
			})
			.pipe(autoprefixer())
			.pipe(minifyCSS({}))
			.pipe(concat('app.min.css'))
			.pipe(header(config.banner, {
				_package: _package
			}))
			.pipe(gulp.dest(dest));
		return stream;
	}
})();