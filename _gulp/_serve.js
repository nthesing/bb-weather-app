(function() {
	/*
	*
	*	Partial gulp file: run
	*	Different gulp run tasks. Starts browsersync.
	*/
	'use strict';

	/*
	*
	*  Include dependencies
	*/
	var gulp = require('gulp');
	var gutil = require('gulp-util');
	var config = require('./_config.js');

	// Function to run enviroments
	function runEnvironment(env, port) {
		var historyApiFallback	= require('connect-history-api-fallback');
		var browserSync = require('browser-sync').get('dev-server');
		
		browserSync.init({
			server: {
				baseDir: env,
			},
			port: config.port,
		});

		gutil.log('### Serving from: ', env);
		gutil.log('### Running on: http://localhost:' + config.port);
	}

	// Serve will trigger sass compiler, bunder vendor script, bundle javascript files and watch tasks
	gulp.task('serve', ['css:maps', 'bundle:vendor', 'bundle:js', 'watch'], function() {
		runEnvironment(config.paths.in.serve);
	});
})();