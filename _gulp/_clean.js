(function() {
	/*
	*
	*	Partial gulp file: clean
	*	A gulp task for removing files and folders.
	*/
	'use strict';

	/*
	*
	*  Include dependencies
	*/
	var gulp = require('gulp');
	var clean = require('gulp-rimraf');
	var config = require('./_config.js');

	/*
	*	Clean task
	*	Will clear directory before
	*/
	gulp.task('clean', function() {
		return gulp.src(config.paths.out.build, {
			read: false
		}).pipe(clean());
	});
})();