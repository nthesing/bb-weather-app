/*
*
* Global Config file
*/
var config = {
	name: 'bb-weather-app',
	slug: 'bbwa',

	port: 3002,
	paths: {
		in: {
			app: 'app',
			serve: './',
			src: 'src/',
			js: 'app/**/*.js',
			css: ['app/components/**/*.sass', 'app/**.sass', 'styles/**/*.sass', 'styles/*.sass'],
		},
		out: {
			js: '/js',
			build: 'dist'
		}
	},

	banner: [
		'/*!\n' +
		' * <%= _package.name %>\n' +
		' * <%= _package.description %>\n' +
		' * <%= _package.url %>\n' +
		' * @author <%= _package.author %>\n' +
		' * @version <%= _package.version %>\n' +
		' * Copyright ' + new Date().getFullYear() + '. <%= _package.license %> licensed.\n' +
		' */',
		'\n'
	].join('')
};

module.exports = config;