#![favicon32.png](https://bitbucket.org/repo/ngk8Ea6/images/4220402862-favicon32.png) Technical Report #

I used Angular in combination with Angular Resource to create this simple weather app. 
This application uses the http://openweathermap.org/api to get the current weather data for 5 different cities in Europe. 
When clicking on the item the forecasted sea level pressure for 5 days will be displayed (only if the data is presented by the API).

The application is served with Gulp. Gulp tasks are also responsible for bundling the javascript files and compiling SASS to CSS. 
Using a Angular filesort package to sort Angular modules dependencies when bundling.

## Application Structure ##
This application uses the 'new' Angular Component structure. Pieces of the application are separated into different components. 
There is only one global factory & filter provided. I choose this setup since its very popular at the moment and also is big advantage moving to Angular 2(/4)
This structure also gives a clear overview of all the components instead of controllers and views separated from each other.

For the styling I choose SASS. Since its really easy to use and gives a lot of functionality. All components have there own .sass file to keep them 
together in 1 folder. For classes in HTML, I used the BEM (block element modifier) technique.
This technique works great with the Angular Components and gives a clean SASS structure.
Created 2 mixins to work with BEM and respond to different devices width, which is good for responsiveness.
 
Thank you for this exercise, it was a pleasure.
