(function() { 
	'use strict';

	// Component: Locations
	// Shows locations based on locations provided by the locations service
	// Gets weather data from api
	var locationsComponent = {
		transclude: true,
		template: `<div class="Locations container">
			<wa-loader ng-if="vm.isLoading"></wa-loader>
			<div ng-if="!vm.isLoading" class="row">
				<wa-location-item item="location" ng-repeat="location in ::vm.locations track by $index"></wa-location-item>
			</div>
		</div>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function($timeout, locationsService, weatherFactory) {
			var vm = this;

			// if loading data show: loader
			vm.isLoading = true;

			getLocationsData();

			/////////////

			/*
			*	Function to get weather data based on locations provided in the location service
			*/
			function getLocationsData() {
				var locations = locationsService.get();

				// Map through locations to join ids to a string
				var ids = locations.map(function(location) {
					return location.id
				}).join(',')

				// Get the weather data 
				weatherFactory.getLocations({id: ids}).$promise.then(function(res) {	
					vm.locations = res.list;

					// Store data in location service
					locationsService.set(res.list);

					// Timeout to give some animation loader effect
					$timeout(function() {
						vm.isLoading = false;
					}, 1000);
				});
			}
		}
	}
	
	angular.module('weatherApp')
		.component('waLocations', locationsComponent);
})();