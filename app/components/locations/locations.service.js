(function() { 
	'use strict';
	
	// Service: Locations
	// Used throughout the application
	angular.module('weatherApp')
		.service('locationsService', locationsService)

	function locationsService() {
		// Define locations for API call
		// Array will be overriden once there is a result from the API
		var locations = [{
				cityName: 'London',
				id: 2643743
			}, {
				cityName: 'Amsterdam',
				id: 2759794
			}, {
				cityName: 'Madrid',
				id: 3117735,
			}, {
				cityName: 'Paris',
				id: 2988507
			}, {
				cityName: 'Monaco',
				id: 2993458
			}
		]

		// Function to check if there is an active item
		// Returns length: 1 is active or 0 none is active
		function checkIfActiveItem() {
			// filter through only return active location
			return locations.filter(function(location) {
				if ( location.active ) return location;
			}).length;
		}

		// Function to set the item to active
		function setActive(item) {
			locations = angular.forEach(locations, function(location) {
				// If location is already active, toggle the active and set active to false
				// Otherwise set location is to active
				if ( location.id == item.id ) {
					location.active = location.active ? false : true;
				} else {
					// set all other locations active state to false
					location.active = false;
				}
			});
		}

		// Function to set weather data to locations once we got result from the API
		function setLocations(_locations) {
			locations = _locations;
		}

		// Function to return all locations 
		function getLocations() {
			return locations;
		}

		///////////////

		return {
			get: getLocations,
			set: setLocations,
			setActive: setActive,
			checkIfActiveItem: checkIfActiveItem
		}
	}

})();