(function() { 
	'use strict';

	// Component: Location Item Forecast
	// Shows sea level pressure forecast for next 5 days
	// Gets forecast data from api
	var locationItemForecast = {
		template: `<section class="Forecast">
				<wa-divider></wa-divider>
				<div class="Forecast__List__Container">
					<p>Sea level pressure for the next 5 days at 09:00 hours</p>
					<wa-loader type="'small'" ng-if="vm.isLoading"></wa-loader>
					<div ng-if="!vm.isLoading">
						<ul class="Forecast__List clearfix">
							<li ng-repeat="forecast in ::vm.forecasts | limitTo:5 track by $index">
								<span>{{forecast.dt | dateformat}}</span>
								<br />
								{{forecast.main.sea_level}}
							</li>
						</ul>
					</div>
				</div>
			</section>`,
		bindings: {
			location: '='
		},
		controllerAs: 'vm',
		controller: function($timeout, weatherFactory) {
			var vm = this;

			// set loading to show loader 
			vm.isLoading = true;

			// function to check for changes on the scope to get the items
			vm.$onChanges = function() {
				getItemForecast();
			}

			// function to get item with 09:00 hours from array and put them in new array
			function parseDays(items) {
				var retVal = [];

				angular.forEach(items, function(item) {

					// max get 5 items
					if ( retVal.length < 5 ) {

						// get hour from time instance
						var hour = moment(item.dt_txt).hour();
						if ( hour === 9 ) {
							retVal.push(item);
						}
					}
				});
				return retVal;
			}

			// function to get item forecast through the weatherFactory
			function getItemForecast() {
				weatherFactory.getForecast({id: vm.location.id}).$promise.then(function(res) {

					// parse result to only get max 5 items
					var list = parseDays(res.list);
					vm.forecasts = list;

					// timeout to give some animation loader effect
					$timeout(function() {
						vm.isLoading = false;
					}, 1000);
				});
			}
		}
	}
	
	angular.module('weatherApp')
		.component('waLocationItemForecast', locationItemForecast);
})();