(function() { 
	'use strict';

	// Component: Single Location Item
	// Shows single location item
	var locationItem = {
		template: `<article class="Location" ng-class="vm.item.active ? 'is-large' : vm.isOtherActive() ? 'is-small' : 'is-medium'">
			<div class="Location__inner img-{{vm.item.name.toLowerCase()}}">
				<a href="#"> 
					<span class="Location__Menu icon icon-menu" ng-click="vm.setActive(vm.item)"></span>
				</a>
				<header class="Location__Heading">
					<h1>{{::vm.item.name}}</h1>
					<span class="Location__date">{{vm.today | date:'dd MMMM yyyy'}}</span>
				</header>
				<section ng-if="!vm.item.active">
					<content class="Location__Content">
						<h2>{{::vm.item.main.temp | number:0 }}<span>&deg;</span></h2>
					</content>
					<wa-divider></wa-divider>
					<footer class="Location__Footer">
						<ul ng-if="!vm.item.active">
							<li title="sunrise"><span class="icon icon-sunrise"></span>{{::vm.item.sys.sunrise * 1000 | date:'hh mm'}} am</li>
							<li title="sunset"><span class="icon icon-sunset"></span>{{::vm.item.sys.sunset * 1000 | date:'hh mm'}} pm</li>
						</ul>
					</footer>
				</section>
				<section ng-if="vm.item.active">
						<wa-location-item-forecast location="vm.item"></wa-location-item-forecast>
				</section>
			</div>
		</article>`,
		bindings: {
			item: '='
		},
		controllerAs: 'vm',
		controller: function(locationsService) {
			var vm = this;

			// Set today's date
			vm.today = new Date();

			// Bind functions to scope
			vm.setActive = setActive;
			vm.isOtherActive = isOtherActive;

			/////////////

			// Fucntion to set location active through the locationService
			function setActive(item) {
				locationsService.setActive(item);
			}

			// Function to see if other locations are active
			// This will apply a 'is-small' class to the component
			function isOtherActive() {
				return locationsService.checkIfActiveItem();
			}
		}
	}
	
	angular.module('weatherApp')
		.component('waLocationItem', locationItem);
})();