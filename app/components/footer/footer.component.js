(function() { 
	'use strict';

	// Component: Footer
	// Shows footer with copyright information and author
	var footerComponent = {
		transclude: true,
		template: `<footer class="Footer">
			<p>&copy; Weather Application | Created by Nick Thesing</p>
		</footer>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;
		}
	}
	
	angular.module('weatherApp')
		.component('waFooter', footerComponent);
})();