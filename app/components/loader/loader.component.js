(function() { 
	'use strict';

	// Component: Loader
	// Shows Loader
	var loaderComponent = {
		template: `<span class="Loader Loader--{{vm.type}}">
			<div class="sk-cube-grid">
				<div class="sk-cube sk-cube1"></div>
				<div class="sk-cube sk-cube2"></div>
				<div class="sk-cube sk-cube3"></div>
				<div class="sk-cube sk-cube4"></div>
				<div class="sk-cube sk-cube5"></div>
			</div></span>`,
		bindings: {
			type: '<'
		},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;
		}
	}
	
	angular.module('weatherApp')
		.component('waLoader', loaderComponent);
})();