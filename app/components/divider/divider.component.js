(function() { 
	'use strict';

	// Component: Divider
	// Shows divider
	var dividerComponent = {
		template: `<div class="Divider"><hr /></div>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;	
		}
	}
	
	angular.module('weatherApp')
		.component('waDivider', dividerComponent);
})();