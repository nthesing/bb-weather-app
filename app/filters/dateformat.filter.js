(function() { 
	'use strict';

	// Filter: Date Format
	// Convert unit to default date and returns a format: 'DD-MM' (day - month)
	angular.module('weatherApp')
		.filter('dateformat', dateformat);

	function dateformat() {
		return function(value) {
			return moment.unix(value).format('DD-MM');
		}
	}
})();