(function() { 
	'use strict';

	// Component: app
	// Application startup, no additional functionality
	var appComponent = {
		transclude: true,
		replace: true,
		template: `
			<main class="WeatherApp">
				<wa-locations class="locations-container"></wa-locations>
				<wa-footer></wa-footer>
			</main>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;
		}
	}
	
	angular.module('weatherApp')
		.component('waApp', appComponent);
})();