(function() { 
	'use strict';
	
	// Module: WeatherApp
	// Dependencies: [ngResource]
	angular.module('weatherApp', ['ngResource'])
})();