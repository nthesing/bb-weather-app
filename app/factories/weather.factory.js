(function() { 
	'use strict';
	
	// Global Factory: Weather
	angular.module('weatherApp')
		.service('weatherFactory', weatherFactory);

	// Inject dependencies
	weatherFactory.$inject = ['$resource'];

	// Weather factory to get the current weather and forecast
	function weatherFactory($resource) {
		var baseUrl = 'http://api.openweathermap.org/data/2.5';
		
		// Multiple locations
		var Locations = $resource(baseUrl + '/group');

		// Forecast for 1 location
		var Forecast = $resource(baseUrl + '/forecast');

		// Default api params (mandatory)
		var defaultParams = {
			units: 'metric',
			appId: 'caa1cd61bad3d002e868cb7edda3597d'
		}

		// Helper function to merge params with defaultparams
		function _merge(params) {
			return angular.merge({}, params, defaultParams); 
		}

		// Function to return weather data for locations provided
		function getLocations(params) {
			params = _merge(params);
			return Locations.get(params);
		}

		// Function to return forecast for locations provided
		function getForecast(params) {
			params = _merge(params);
			return Forecast.get(params);
		}

		/////////////////

		return {
			getLocations: getLocations,
			getForecast: getForecast
		}
	}

})();