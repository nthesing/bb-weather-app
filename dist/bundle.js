/*!
 * bb-weather-app
 * Weather application to show weather information for different cities.
 * 
 * @author Nick Thesing
 * @version 1.0.0
 * Copyright 2017. ISC licensed.
 */
(function() { 
	'use strict';
	
	// Module: WeatherApp
	// Dependencies: [ngResource]
	angular.module('weatherApp', ['ngResource'])
})();
(function() { 
	'use strict';

	// Component: app
	// Application startup, no additional functionality
	var appComponent = {
		transclude: true,
		replace: true,
		template: `
			<main class="WeatherApp">
				<wa-locations class="locations-container"></wa-locations>
				<wa-footer></wa-footer>
			</main>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;
		}
	}
	
	angular.module('weatherApp')
		.component('waApp', appComponent);
})();
(function() { 
	'use strict';

	// Component: Divider
	// Shows divider
	var dividerComponent = {
		template: `<div class="Divider"><hr /></div>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;	
		}
	}
	
	angular.module('weatherApp')
		.component('waDivider', dividerComponent);
})();
(function() { 
	'use strict';

	// Component: Footer
	// Shows footer with copyright information and author
	var footerComponent = {
		transclude: true,
		template: `<footer class="Footer">
			<p>&copy; Weather Application | Created by Nick Thesing</p>
		</footer>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;
		}
	}
	
	angular.module('weatherApp')
		.component('waFooter', footerComponent);
})();
(function() { 
	'use strict';

	// Component: Loader
	// Shows Loader
	var loaderComponent = {
		template: `<span class="Loader Loader--{{vm.type}}">
			<div class="sk-cube-grid">
				<div class="sk-cube sk-cube1"></div>
				<div class="sk-cube sk-cube2"></div>
				<div class="sk-cube sk-cube3"></div>
				<div class="sk-cube sk-cube4"></div>
				<div class="sk-cube sk-cube5"></div>
			</div></span>`,
		bindings: {
			type: '<'
		},
		controllerAs: 'vm',
		controller: function() {
			var vm = this;
		}
	}
	
	angular.module('weatherApp')
		.component('waLoader', loaderComponent);
})();
(function() { 
	'use strict';

	// Component: Location Item Forecast
	// Shows sea level pressure forecast for next 5 days
	// Gets forecast data from api
	var locationItemForecast = {
		template: `<section class="Forecast">
				<wa-divider></wa-divider>
				<div class="Forecast__List__Container">
					<p>Sea level pressure for the next 5 days at 09:00 hours</p>
					<wa-loader type="'small'" ng-if="vm.isLoading"></wa-loader>
					<div ng-if="!vm.isLoading">
						<ul class="Forecast__List clearfix">
							<li ng-repeat="forecast in ::vm.forecasts | limitTo:5 track by $index">
								<span>{{forecast.dt | dateformat}}</span>
								<br />
								{{forecast.main.sea_level}}
							</li>
						</ul>
					</div>
				</div>
			</section>`,
		bindings: {
			location: '='
		},
		controllerAs: 'vm',
		controller: function($timeout, weatherFactory) {
			var vm = this;

			// set loading to show loader 
			vm.isLoading = true;

			// function to check for changes on the scope to get the items
			vm.$onChanges = function() {
				getItemForecast();
			}

			// function to get item with 09:00 hours from array and put them in new array
			function parseDays(items) {
				var retVal = [];

				angular.forEach(items, function(item) {

					// max get 5 items
					if ( retVal.length < 5 ) {

						// get hour from time instance
						var hour = moment(item.dt_txt).hour();
						if ( hour === 9 ) {
							retVal.push(item);
						}
					}
				});
				return retVal;
			}

			// function to get item forecast through the weatherFactory
			function getItemForecast() {
				weatherFactory.getForecast({id: vm.location.id}).$promise.then(function(res) {

					// parse result to only get max 5 items
					var list = parseDays(res.list);
					vm.forecasts = list;

					// timeout to give some animation loader effect
					$timeout(function() {
						vm.isLoading = false;
					}, 1000);
				});
			}
		}
	}
	
	angular.module('weatherApp')
		.component('waLocationItemForecast', locationItemForecast);
})();
(function() { 
	'use strict';

	// Component: Single Location Item
	// Shows single location item
	var locationItem = {
		template: `<article class="Location" ng-class="vm.item.active ? 'is-large' : vm.isOtherActive() ? 'is-small' : 'is-medium'">
			<div class="Location__inner img-{{vm.item.name.toLowerCase()}}">
				<a href="#"> 
					<span class="Location__Menu icon icon-menu" ng-click="vm.setActive(vm.item)"></span>
				</a>
				<header class="Location__Heading">
					<h1>{{::vm.item.name}}</h1>
					<span class="Location__date">{{vm.today | date:'dd MMMM yyyy'}}</span>
				</header>
				<section ng-if="!vm.item.active">
					<content class="Location__Content">
						<h2>{{::vm.item.main.temp | number:0 }}<span>&deg;</span></h2>
					</content>
					<wa-divider></wa-divider>
					<footer class="Location__Footer">
						<ul ng-if="!vm.item.active">
							<li title="sunrise"><span class="icon icon-sunrise"></span>{{::vm.item.sys.sunrise * 1000 | date:'hh mm'}} am</li>
							<li title="sunset"><span class="icon icon-sunset"></span>{{::vm.item.sys.sunset * 1000 | date:'hh mm'}} pm</li>
						</ul>
					</footer>
				</section>
				<section ng-if="vm.item.active">
						<wa-location-item-forecast location="vm.item"></wa-location-item-forecast>
				</section>
			</div>
		</article>`,
		bindings: {
			item: '='
		},
		controllerAs: 'vm',
		controller: function(locationsService) {
			var vm = this;

			// Set today's date
			vm.today = new Date();

			// Bind functions to scope
			vm.setActive = setActive;
			vm.isOtherActive = isOtherActive;

			/////////////

			// Fucntion to set location active through the locationService
			function setActive(item) {
				locationsService.setActive(item);
			}

			// Function to see if other locations are active
			// This will apply a 'is-small' class to the component
			function isOtherActive() {
				return locationsService.checkIfActiveItem();
			}
		}
	}
	
	angular.module('weatherApp')
		.component('waLocationItem', locationItem);
})();
(function() { 
	'use strict';

	// Component: Locations
	// Shows locations based on locations provided by the locations service
	// Gets weather data from api
	var locationsComponent = {
		transclude: true,
		template: `<div class="Locations container">
			<wa-loader ng-if="vm.isLoading"></wa-loader>
			<div ng-if="!vm.isLoading" class="row">
				<wa-location-item item="location" ng-repeat="location in ::vm.locations track by $index"></wa-location-item>
			</div>
		</div>`,
		bindings: {},
		controllerAs: 'vm',
		controller: function($timeout, locationsService, weatherFactory) {
			var vm = this;

			// if loading data show: loader
			vm.isLoading = true;

			getLocationsData();

			/////////////

			/*
			*	Function to get weather data based on locations provided in the location service
			*/
			function getLocationsData() {
				var locations = locationsService.get();

				// Map through locations to join ids to a string
				var ids = locations.map(function(location) {
					return location.id
				}).join(',')

				// Get the weather data 
				weatherFactory.getLocations({id: ids}).$promise.then(function(res) {	
					vm.locations = res.list;

					// Store data in location service
					locationsService.set(res.list);

					// Timeout to give some animation loader effect
					$timeout(function() {
						vm.isLoading = false;
					}, 1000);
				});
			}
		}
	}
	
	angular.module('weatherApp')
		.component('waLocations', locationsComponent);
})();
(function() { 
	'use strict';
	
	// Service: Locations
	// Used throughout the application
	angular.module('weatherApp')
		.service('locationsService', locationsService)

	function locationsService() {
		// Define locations for API call
		// Array will be overriden once there is a result from the API
		var locations = [{
				cityName: 'London',
				id: 2643743
			}, {
				cityName: 'Amsterdam',
				id: 2759794
			}, {
				cityName: 'Madrid',
				id: 3117735,
			}, {
				cityName: 'Paris',
				id: 2988507
			}, {
				cityName: 'Monaco',
				id: 2993458
			}
		]

		// Function to check if there is an active item
		// Returns length: 1 is active or 0 none is active
		function checkIfActiveItem() {
			// filter through only return active location
			return locations.filter(function(location) {
				if ( location.active ) return location;
			}).length;
		}

		// Function to set the item to active
		function setActive(item) {
			locations = angular.forEach(locations, function(location) {
				// If location is already active, toggle the active and set active to false
				// Otherwise set location is to active
				if ( location.id == item.id ) {
					location.active = location.active ? false : true;
				} else {
					// set all other locations active state to false
					location.active = false;
				}
			});
		}

		// Function to set weather data to locations once we got result from the API
		function setLocations(_locations) {
			locations = _locations;
		}

		// Function to return all locations 
		function getLocations() {
			return locations;
		}

		///////////////

		return {
			get: getLocations,
			set: setLocations,
			setActive: setActive,
			checkIfActiveItem: checkIfActiveItem
		}
	}

})();
(function() { 
	'use strict';
	
	// Global Factory: Weather
	angular.module('weatherApp')
		.service('weatherFactory', weatherFactory);

	// Inject dependencies
	weatherFactory.$inject = ['$resource'];

	// Weather factory to get the current weather and forecast
	function weatherFactory($resource) {
		var baseUrl = 'http://api.openweathermap.org/data/2.5';
		
		// Multiple locations
		var Locations = $resource(baseUrl + '/group');

		// Forecast for 1 location
		var Forecast = $resource(baseUrl + '/forecast');

		// Default api params (mandatory)
		var defaultParams = {
			units: 'metric',
			appId: 'caa1cd61bad3d002e868cb7edda3597d'
		}

		// Helper function to merge params with defaultparams
		function _merge(params) {
			return angular.merge({}, params, defaultParams); 
		}

		// Function to return weather data for locations provided
		function getLocations(params) {
			params = _merge(params);
			return Locations.get(params);
		}

		// Function to return forecast for locations provided
		function getForecast(params) {
			params = _merge(params);
			return Forecast.get(params);
		}

		/////////////////

		return {
			getLocations: getLocations,
			getForecast: getForecast
		}
	}

})();
(function() { 
	'use strict';

	// Filter: Date Format
	// Convert unit to default date and returns a format: 'DD-MM' (day - month)
	angular.module('weatherApp')
		.filter('dateformat', dateformat);

	function dateformat() {
		return function(value) {
			return moment.unix(value).format('DD-MM');
		}
	}
})();