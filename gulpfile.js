(function() {
	/*
	*	Gulp configuration file for project
	*/
	'use strict';

	/*
	*  Include dependencies
	*/
	var gulp = require('gulp');
	var config = require('./_gulp/_config.js');
	var gutil = require('gulp-util');
	var browserSync = require('browser-sync').create('dev-server');

	/*
	*  Require gulp task partials
	*/
	require('require-dir')('./_gulp');

	gulp.task('default', function () {
		console.log("No default tasks defined, use: 'gulp serve' to start the application");
	});
})();