#![favicon32.png](https://bitbucket.org/repo/ngk8Ea6/images/4220402862-favicon32.png)     Weather Application#


Simple weather application that shows current weather information for 5 different cities. Clicking on an item shows the sea level pressure forecast for the next 5 days.


**Please note**: this application is required to open from the file system. No AJAX implementations have been made. 


## Build with ##
* Angular (v1.6.4)
* Angular Resource (v1.6.4)
* Bootstrap (v3.3.7)
* NPM
* Bower
* Gulp


## Getting started ##

Clone application and move into the bb-weather-app folder.

#### Install all node dependencies ####

```
npm install
```

#### Install all bower dependencies #### 
```
bower install
```

#### Run application to serve in browser and compile javascript & css. ####

```
gulp serve
```

Running the gulp serve task will open the application in the browser automatically.

### Assets ###
* Unsplash [https://unsplash.com/](https://unsplash.com/)
* Icomoon [https://icomoon.io](https://icomoon.io)